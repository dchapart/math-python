"""
Fonctions mathématiques de base
"""
from typing import List
import urllib.request
from collections import defaultdict


def sqrt(number: int, tolerance: float = 1e-10) -> int:
    if number < 0.0:
        raise ValueError("Square root not defined for negative numbers.")
    guess = number
    while abs(guess * guess - number) > tolerance:
        guess = (guess + number / guess) / 2
    return guess


def average(x: List[float | int]):
    return sum(x) / float(len(x)) if x else 0


from itertools import (accumulate, chain)
from operator import mul


# factorial :: Integer
def factorial(n):
    return list(
        accumulate(chain([1], range(1, 1 + n)), mul)
    )[-1]


def pgcd(u: int, v: int):
    return pgcd(v, u % v) if v else abs(u)


def median(aray) -> int:
    srtd = sorted(aray)
    alen = len(srtd)
    return 0.5 * (srtd[(alen - 1) // 2] + srtd[alen // 2])


def pow(x, y):
    return float(x) ** float(y)


from typing import Collection


def fibonacci(until: float) -> Collection[int]:
    n1 = 0
    n2 = 1
    numbers = [n1, n2]
    for loop_count in range(2, until):
        next = n1 + n2
        numbers.append(loop_count)

        n1 = n2
        n2 = next
    return numbers


def fsum(x: list[float]) -> "Float":
    if len(x) > 0:
        return x[0] + fsum(x[1:])
    return 0


def crible_eratosthene(n : int) -> List[int] :
    is_prime = [True] * (n + 1) # init array of true values

    is_prime[0] = is_prime[1] = False # 0 and 1 are prime numbers

    for num in range(2, int(n ** 0.5) + 1):
        if is_prime[num]:
            for multiple in range(num * num, n + 1, num):
                is_prime[multiple] = False

    # return only prime numbers
    primes = [num for num in range(2, n + 1) if is_prime[num]]

    return primes


# fonction anagramme qui prend un texte en paramètre et qui retourne une liste de mots qui sont des anagrammes
# du texte passé en paramètre
# exemple : anagramme("le developpement c'est génial") retourne ['le', 'c\'est', 'le', 'genial']
def anagramme(text: str) -> list[str]:
    # On récupère les mots du texte
    words = text.split(" ")
    # On trie les lettres de chaque mot
    sorted_words = ["".join(sorted(word)) for word in words]
    # On crée un dictionnaire qui contient en clé les mots triés et en valeur les mots non triés
    anagrams = defaultdict(list)
    for word, sorted_word in zip(words, sorted_words):
        anagrams[sorted_word].append(word)
    # On retourne les valeurs du dictionnaire
    return list(anagrams.values())
